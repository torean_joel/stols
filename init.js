// init the application messages from the server
(function () {
  // init the bg img
  initBg();

  introMessage();
  // messages come here
  readDataMessages();
  // ending message
  endMessage();

  // we need to be able to wait for the aboe
  setTimeout(() => {
    Reveal.initialize();
  }, 3000)
})();
