// helpers for the application

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyDqE8uOH0WN4rI58dE4mUxZzLjRT3lY0tg",
  authDomain: "farewell-e3b20.firebaseapp.com",
  projectId: "farewell-e3b20",
  storageBucket: "farewell-e3b20.appspot.com",
  messagingSenderId: "667931723",
  appId: "1:667931723:web:48d9c68c3d1875ac7fa9ce"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// init database
var database = firebase.database();

// write to databse
function writeData({ name, message }) {
  var msgRef = firebase.database().ref('messages');
  var newMsgRef = msgRef.push();
  newMsgRef.set({
    name,
    message,
  });
}

/**
 * messages helper
 */
function readDataMessages() {
  var messagesRef = firebase.database().ref('messages');
  var messageIntroRef = firebase.database().ref('messageIntro');
  // console.info('messagesRef', messagesRef)
  messageIntroRef.once('value', (messageIntroSnap) => {
    const { heading, message } = messageIntroSnap.val();
    if (!heading) return false;
    if (!message) return false;
    renderElm('#messagesIntro', heading, message, false);
    // get messages
    messagesRef.once('value', (messagesSnap) =>{
      const messagesData = messagesSnap.val();
      Object.keys(messagesData).forEach((item) => {
        const currentItem = messagesData[item];
        renderElm('#messages', currentItem.name, currentItem.message, true);
      });
    });
  });
}

/**
 * intro message helper
 */
function introMessage() {
  var introMsgRef = firebase.database().ref('intro');
  introMsgRef.once('value', (snapshot) =>{
    const { heading, message } = snapshot.val();
    if (!heading) return false;
    if (!message) return false;
    renderElm('#intro', heading, message, false);
  });
}

/**
 * intro message helper
 */
function initBg() {
  var introMsgRef = firebase.database().ref('bg');
  introMsgRef.on('value', (snapshot) =>{
    const data = snapshot.val();
    // get all the slides and add image to it
    renerBg('#main-render', data);
  });
}

/**
 * ending message helper
 */
function endMessage() {
  var endMsgRef = firebase.database().ref('end');
  // console.info('endMsgRef', endMsgRef)
  endMsgRef.once('value', (snapshot) =>{
    const { heading, message } = snapshot.val();
    if (!heading) return false;
    if (!message) return false;
    renderElm('#end', heading, message, false);
  });
}

/**
 * this will render the message on the screen
 * @param {DOM} target the target dom element message
 * @param {string} heading the string for the heading of the message
 * @param {string} body the string onf the body for the message 
 * @param {boolean} hasChildren this will render the list under eachother 
 */
function renderElm(target, heading, body, hasChildren = false) {
  const parentElm = document.querySelector(target);
  const frameHeading = document.createElement('h2');
  const frameBody = document.createElement('p');
  if (!hasChildren) {
    frameHeading.append(heading);
    frameBody.append(body);
    // add to section
    parentElm.append(frameHeading);
    parentElm.append(frameBody);
  } else {
    const frameSection = document.createElement('section');
    frameHeading.append(heading);
    frameBody.append(body);
    // add to section
    frameSection.append(frameHeading);
    frameSection.append(frameBody);
    // add to body
    parentElm.append(frameSection);
  }
}

/**
 * this iwll render a image on the background of target
 * @param {DOM} target the parent elm that will render the image as its bg
 * @param {string} url the url of the img 
 */
function renerBg(target, url) {
  const parentElm = document.querySelector(target);
  //set the bg to the image
  parentElm.style.background = `url(${url})`;
  parentElm.style.backgroundSize = `cover`;
  parentElm.style.backgroundRepeat = `no-repeat`;
}