const inputName = document.querySelector('#input-name');
const inputMessage = document.querySelector('#input-message');
const submitBtn = document.querySelector('#submit-btn');
const thankYou = document.querySelector('#thankyou');
const formSection = document.querySelector('#formSection');
const limitMsg = document.querySelector('.limitMsg');

submitBtn.addEventListener('click', (event) => {
  sendForm(event);
});

function sendForm(event) {
  event.preventDefault();
  // from the helpers but this sends data o the database
  if (!inputName.value) return false;
  if (!inputMessage.value) return false;

  writeData({
    name: inputName.value,
    message: inputMessage.value
  })

  // here we need to clear the form and show a message
  inputName.value = "";
  inputMessage.value = "";

  formSection.style.display = 'none';
  limitMsg.style.display = 'none';
  thankYou.style.display = 'inline';
}